===========================================
Revisão de programação procedural em Python
===========================================

.. toctree::

   Aula teórica <teorica>
   Aula prática <pratica>
   Soluções da aula prática <pratica_resolvida>
