import cenario as cn
import fisica  as fs
from objetos import Bola, Lampada, Mesa

mesa = Mesa()
lampada_central = Lampada()

def setup():
    size(cn.pixels(Mesa.largura), cn.pixels(Mesa.altura))
    
def mouseClicked():
    pass
    
def draw():
    mesa.desenhar()
    lampada_central.desenhar()
