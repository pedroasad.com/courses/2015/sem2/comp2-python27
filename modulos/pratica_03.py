class Vetor:
	def __init__(v, x, y, z):
		v.x = x
		v.y = y
		v.z = z

class Pessoa:
	def __init__(p, nome, data_nascimento):
		p.nome = nome
		p.data_nascimento = data_nascimento

	def __repr__(p):
		return unicode(p.nome) + ', ' + str(p.data_nascimento)

def soma_vetorial(u, v):
	return Vetor(u.x + v.x, u.y + v.y, u.z + v.z)

def subtracao_vetorial(u, v):
	return Vetor(u.x - v.x, u.y - v.y, u.z - v.z)

def produto_escalar_vetor(k, v):
	return Vetor(k * v.x, k * v.y, k * v.z)

def produto_interno_vetorial(u, v):
	return u.x * v.x + u.y * v.y + u.z * v.z

import datetime as dt
import random as rd

nomes = [
	'Homer',
	'Neo',
	'Bilbo',
	'Luke',
	'Tony',
	'Natasha',
	'Trinity',
	'Marge',
	'Lea',
	'Saori'
]

datas = [
	dt.date(1964, 8, 15), 
	dt.date(1986, 2, 9), 
	dt.date(1966, 2, 17), 
	dt.date(1979, 9, 21), 
	dt.date(1962, 4, 19), 
	dt.date(1986, 8, 29), 
	dt.date(1991, 5, 30), 
	dt.date(1993, 1, 23),
	dt.date(1975, 10, 5), 
	dt.date(1975, 7, 28)
]

pessoas = []
for n, d in zip(nomes, datas):
	pessoas.append(Pessoa(n, d))

def loteria():
	return rd.choice(pessoas)

def datas_aleatorias(n, ano_i, ano_f):
	datas = []

	for i in range(n):
		ano = rd.randint(ano_i, ano_f)
		mes = rd.randint(1, 12)
		dia = rd.randint(1, 28)

		datas.append(dt.date(ano, mes, dia))

	return datas

def datas_extremas(datas):
	menor_data = None
	maior_data = None

	for d in datas:
		if menor_data is None and maior_data is None:
			menor_data = d
			maior_data = d
		else:
			if d < menor_data:
				menor_data = d

			if d > maior_data:
				maior_data = d

	return menor_data, maior_data

def idade(pessoa):
	idade_dias = dt.date.today() - pessoa.data_nascimento
	return idade_dias.days / 365

def pessoas_mais_jovens(pessoas):
	jovens = []
	for p in pessoas:
		if jovens:
			if idade(p) < idade(jovens[0]):
				jovens = [p]
			elif idade(p) == idade(jovens[0]):
				jovens.append(p)
		else:
			jovens = [p]
	return jovens

def pessoas_mais_velhas(pessoas):
	velhas = []
	for p in pessoas:
		if velhas:
			if idade(p) > idade(velhas[0]):
				velhas = [p]
			elif idade(p) == idade(velhas[0]):
				velhas.append(p)
		else:
			velhas = [p]
	return velhas

def idade_media(pessoas):
	idades = []
	for p in pessoas:
		idades.append(idade(p))
	return sum(idades) / len(idades)

def pessoas_medianas(pessoas, tolerancia=0):
	im = idade_media(pessoas)
	medianas = []
	for p in pessoas:
		if abs(idade(p) - im) <= tolerancia:
			medianas.append(p)
	return medianas

def pessoas_abaixo_media(pessoas):
	im = idade_media(pessoas)
	jovens = []
	for p in pessoas:
		if idade(p) <= im:
			jovens.append(p)
	return jovens

def pessoas_acima_media(pessoas):
	im = idade_media(pessoas)
	velhas = []
	for p in pessoas:
		if idade(p) >= im:
			velhas.append(p)
	return velhas
