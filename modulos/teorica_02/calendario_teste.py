from calendario import *

def testeDuasDatasEspecificas():
	assert diaDaSemana(24, 3, 2015) == 3
	assert diaDaSemana(25, 3, 2015) == 4

def testeAnoBissexto():
	assert ehAnoBissexto(2013) == False
	assert ehAnoBissexto(2014) == False
	assert ehAnoBissexto(2015) == False
	assert ehAnoBissexto(2016) == True

	assert ehAnoBissexto(2000) == True
	assert ehAnoBissexto(2100) == False
	assert ehAnoBissexto(2200) == False
	assert ehAnoBissexto(2300) == False

testeAnoBissexto()
testeDuasDatasEspecificas()
