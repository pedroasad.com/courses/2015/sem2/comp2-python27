"""
Criar uma biblioteca de classes para representar funções de uma ou mais
variáveis. Composições de funções geram novoso objetos compatíveis com o
protocolo de função:

__call__ deve estar definido e levar em conta *args e **kwargs na avaliação;
então, se os parâmetros são passados em *args, considera-se a ordem das
variáveis na definição da função — se for possível estabelecer alguma ordem —
e, caso contrário, leva-se em conta **kwargs

derivada, integral e integral_definida devem estar disponíveis
"""
