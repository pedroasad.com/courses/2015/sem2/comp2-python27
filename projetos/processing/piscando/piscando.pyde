fps = 20
pontos = []
velocidades = []

def setup():
    size(800, 500)
    frameRate(fps)
    
    for i in range(20):
        list.append(pontos, [random(width), random(height)])
        list.append(velocidades, [random(10) + 1, random(10) + 1])
    
def draw():
    background(0)
    
    fill(255)
    stroke(255)
    strokeWeight(6)
    for i in range(len(pontos)):
        point(pontos[i][0], pontos[i][1])
        
        pontos[i][0] += velocidades[i][0]
        pontos[i][1] += velocidades[i][1]
        
        if pontos[i][0] >= width:
            velocidades[i][0] *= -1
            
        if pontos[i][1] >= height:
            velocidades[i][1] *= -1
    
