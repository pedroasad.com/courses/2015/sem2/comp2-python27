class Bola:
    raio = 15
    
    cores = [
        (255, 255, 255), # Branco 
        (255,  50,  50), # Vermelho
        (255, 220,  36), # Amarelo
        ( 50, 150,  50), # Verde
        (150, 100,  50), # Marrom
        ( 50,  50, 255), # Azul
        (255, 150, 150), # Rosa
        (  0,   0,   0)] # Preto
    
    def __init__(self, num, pos):
        self.num = num
        self.pos = pos
        self.vel = PVector(0.0, 0.0)
        self.acl = PVector(0.0, 0.0)
        
    def cor(self):
        return Bola.cores[self.num]
        
    def desenhar(self):
        noStroke()
        c = self.cor()
        
        fill(c[0], c[1], c[2])
        ellipseMode(CENTER)
        ellipse(self.pos.x, self.pos.y, 2 * Bola.raio, 2 * Bola.raio)
        
        if self.num > 0:
            fill(255)
            ellipse(self.pos.x, self.pos.y, 1.25 * Bola.raio, 1.25 * Bola.raio)
            
            fill(0)
            textMode(CENTER)
            textAlign(CENTER)
            text(str(self.num), self.pos.x, self.pos.y, 1.25 * Bola.raio, 1.25 * Bola.raio)
            
    def impulso(self, velocidade):
        self.vel += velocidade
        
    def atualizar(self, delta_tempo):
        p = self.pos
        v = self.vel
        a = self.vel
        t = delta_tempo
        
        if v.angleBetween(v + a * t) < PI:
            self.pos += v * t + a * t**2 / 2
            self.vel += a * t
            self.acl = -40 * v / v.mag()
        else:
            self.pos += v * t + a * t**2 / 2
            self.vel = PVector(0.0, 0.0)
            self.acl = PVector(0.0, 0.0)

class Mesa:
    def __init__(self):
        self.bolas = []
        self.mortas = []
    
    def desenhar(self):
        fill(0, 75, 0)
        rectMode(CENTER)
        rect(width / 2, height / 2, width, height)
        
        borda = Bola.raio
        raio_cacapa = 1.2 * Bola.raio
        
        stroke(0)
        strokeWeight(2)
        noFill()
        rect(width / 2, height / 2, width - 2 * borda, height - 2 * borda)
        
        fill(0)
        for x, y, a, b in [(0, 0, 1, 1), (width / 2, 0, 0, 1), (width, 0, -1, 1), (width, height, -1, -1), (width / 2, height, 0, -1), (0, height, 1, -1)]:
            ellipse(x + a * raio_cacapa, y + b * raio_cacapa, 2 * raio_cacapa, 2 * raio_cacapa)

        for b in self.bolas:
            if b not in self.mortas:
                b.desenhar()
            
    def parada(self):
        return True
    
    def arrumar(self, tipo):
        if tipo == 'tradicional':
            posicoes = [
            (      0        ,        0        ),
            
            (    cos(PI / 6), -    sin(PI / 6)),
            (    cos(PI / 6),      sin(PI / 6)),
    
            (2 * cos(PI / 6), -2 * sin(PI / 6)),
            (2 * cos(PI / 6),        0        ),
            (2 * cos(PI / 6),  2 * sin(PI / 6)),
            
            (3 * cos(PI / 6), -3 * sin(PI / 6)),
            (3 * cos(PI / 6), -    sin(PI / 6)),
            (3 * cos(PI / 6),      sin(PI / 6)),
            (3 * cos(PI / 6),  3 * sin(PI / 6)),
            
            (4 * cos(PI / 6), -2 * sin(PI / 6)),
            (4 * cos(PI / 6), -    sin(PI / 6)),
            (4 * cos(PI / 6),        0        ),
            (4 * cos(PI / 6),      sin(PI / 6)),
            (4 * cos(PI / 6),  2 * sin(PI / 6))
            ]
            
            self.bolas.append(Bola(0, [width / 4, height / 2]))
            
            for i in range(1, 8):
                b = Bola(i, [3 * width / 4 + 2 * Bola.raio * posicoes[i - 1][0], height / 2 + 2 * Bola.raio * posicoes[i - 1][1]])
                self.bolas.append(b)
        
            self.mortas = []
            
class Taco:
    def __init__(self, mesa):
        self.mesa = mesa
        
    def desenhar(self):
        stroke(230)
        strokeWeight(2)
        line(mouseX, mouseY, self.mesa.bolas[0].pos.x, self.mesa.bolas[0].pos.y)
        
    def disparar(self):
        self.mesa.taco.disparar()
