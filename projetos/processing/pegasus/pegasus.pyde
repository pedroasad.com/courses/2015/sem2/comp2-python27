meteoros = []

def meteoro():
    tam_min =  2 
    tam_max = 20
    vel_min =  1
    vel_max =  2
    
    met = []
    if random(1.0) < 0.5:
        return [random(width), 0, random(tam_min, tam_max + 1), random(vel_min, vel_max + 1)]
    else:
        return [0, random(height), random(tam_min, tam_max + 1), random(vel_min, vel_max + 1)]

def setup():
    size(800, 600)
    background(0)
    
    global meteoros
    meteoros = [meteoro() for i in range(15)]
    
def draw():
    fill(0, 10)
    rect(0, 0, width, height)
    
    noStroke()
    for m in meteoros:
        for r in range(2, int(1.5 * m[2])):
            fill(255, 50, 0, (float(m[2] - r) / (1.5 * m[2]))**2 * 255)
            ellipse(m[0], m[1], r, r)
        m[0] += m[3]# - 2 * float(mouseX - m[0]) / width
        m[1] += m[3] - 2 * float(mouseY - m[1]) / height
        if m[0] >= width or m[1] >= height:
            met = meteoro()
            m[0], m[1], m[2], m[3] = met
