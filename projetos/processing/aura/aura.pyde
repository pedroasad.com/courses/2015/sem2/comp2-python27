def setup():
    size(800, 600)
    
def draw():
    background(0)
    
    raio = 50
    raio_aura = 4 * raio
    
    noStroke()
    for r in range(raio_aura, raio - 1, -1):
        fill(255, 255 * (float(raio_aura - r) / (raio_aura - raio))**3)
        ellipse(mouseX, mouseY, r, r)
