#Time tracker#

##Missão##
Entender e controlar a maneira como uso meu tempo.

##Entendendo o domínio##

Quatro aspectos principais:
	1. Identificar as características das atividades e os modos de alocação do tempo existentes (descrição domínio)
	1. Visualização da distribuição de tempo
	1. Planejamento da distribuição de tempo
	1. Visualização da discrepância visualização versus planejamento

###1. Tipos de atividades e modos de alocação do tempo###

	* Toda atividade tem um nome e uma descrição
	* Quais são minhas atividades?
	* Algumas atividades são passíveis de sobreposição?
	* Uma atividade é desejada ou inevitável?
	* Uma atividade é consequência?
	* Uma atividade pertence a um grupo de atividades relacionadas?
	* Quanto tempo uma atividade necessita?
	* Uma atividade acaba após um número de sessões?
	* O valor de uma atividade está em ser completada ou praticada com regularidade?
	* A atividade tem dias e horários certos para acontecer ou não?
	* É importante que uma atividade ocorra na hora programada ou não?
	* É importante que uma atividade dure o temo estimado ou não?
	* Uma atividade depende sequencialmente de outra?
	* Uma atividade depende concomitantemente de outra?
	* Uma atividade tem tempo conhecidamente flutuante?
	* Umas atividades são mais importantes que outras
	* Algumas atividades tem mais prioridade que outras
	* Algumas atividades podem ser adiadas
	* Algumas atividades são opcionais
	* As categorias de atividades induzem uma ou mais ordenações parciais ou totais?
	* O comportamento de uma atividade é melhor determinado por sua categoria ou por seus atributos (conjunto de restrições)?

###2. Como se distribui o tempo?###

	* Quanto tempo tem sido investido em cada atividade?
	* Quais atividades são praticadas regularmente e esporadicamente?

###3. Quero planejar meu tempo###

	Quais são os workflows possíveis?

	1. Encaixar uma ou mais atividades em um planejamento existente
		* Todo planejamento válido por ao menos um dia deve ser salvo?
		* Não há tempo contíguo para a atividade: repartir, encolher ou desistir?

	1. Criar um planejamento aleatório para um conjunto de atividades bem especificadas

	1. Fixar algumas atividades e distribuir as demais
		* Quais serão os espaços vazios?
		* Distribuir as atividades regulares de maneira espaçada?

	1. Escolha de atividades aleatórias
		* De um grupo de atividades que cumprem papéis redundantes (mutuamente exclusivas)
		* De uma lista de desejos

	* Quero encontrar um tempo vago para uma atividade
		* Busca por restrições
		* Busca aproximada -- sugestões de quasi-soluções possíveis sob ajuste

###4. Como a realidade se desvia do planejamento###

	1. Análise das discrepâncias (modo passivo)
	1. Alerta sobre discrepâncias (modo ativo)
		* Quais atividades estão abandonadas?
		* Uma atividade recebe a atenção pretendida, mais ou menos?
	1. Aprendizado sobre o passado
		* Será que com base em um histórico planejamento x realidade posso saber que uma atividade está mal planejada?
		* Quais atividades são conhecidas por meio de experiências passadas?
		* Atividades podem mudar de característica -- então parece melhor que sejam caracterizadas por um conjunto de restrições e não por uma classe distinta

	VARIAÇÂO
		* Planejamentos para fases específicas do ano
		* 

##Modelo##

A entidade central não é atividade, mas a restrição. Uma atividade é
simplesmente um nome associado a um conjunto de restrições. Cada tipo de
restrição avalia se uma determinada alocação é apropriada ou não. Uma atividade
está bem alocada quando satisfaz ao maior número possível de restrições. A
satisfabilidade deve ser nebulosa, com 0.0 = totalmente insatisfeito, 1.0 =
totalmente satisfeita e todos os outros valores indicando uma gradação de
adequação.

##Visualização##

##Planejamento##

##Entrada de dados##

A interação com o usuário deve ser, sobretudo, minimalista. Mudanças nos
atributos de uma restrição ou na própria lista de restrições de uma atividade
devem ser propostos de maneira automática pela aplicação, quando detectados
desacordos insistentes entre realidade e planejamento.
