# coding: utf-8

atividades = []

list.append(atividades, {
    'nome'       : 'Cálculo 1',
    'dias'       : [2, 4, 6],
    'horario'    : (8, 10),
    'periodo'    : ((2, 3, 2015), (25, 7, 2015)),
    'categorias' : ['aula', 'faculdade']
})

list.append(atividades, {
    'nome'       : 'Física 1',
    'dias'       : [2, 4, 6],
    'horario'    : (10, 12),
    'periodo'    : ((2, 3, 2015), (25, 7, 2015)),
    'categorias' : ['aula', 'faculdade']
})

list.append(atividades, {
    'nome'       : 'Computação 1',
    'dias'       : [3, 5],
    'horario'    : (13, 15),
    'periodo'    : ((2, 3, 2015), (25, 7, 2015)),
    'categorias' : ['aula', 'faculdade']
})

list.append(atividades, {
    'nome'       : 'Aula de baixo',
    'dias'       : [3],
    'horario'    : (19, 20),
    'periodo'    : ((19, 7, 2013), None),
    'categorias' : ['aula', 'lazer', 'musica']
})

list.append(atividades, {
    'nome'       : 'Comunidança',
    'dias'       : [3, 5],
    'horario'    : (17, 18),
    'periodo'    : ((2, 3, 2015), (25, 7, 2015)),
    'categorias' : ['aula', 'lazer', 'social']
})

list.append(atividades, {
    'nome'       : 'Ensaio',
    'dias'       : [6],
    'horario'    : (10,15),
    'periodo'    : ((13, 11, 2014), None),
    'categorias' : ['musica']
})

list.append(atividades, {
    'nome'       : 'Academia',
    'dias'       : [2, 4, 5, 6],
    'horario'    : (20, 21),
    'periodo'    : ((20, 5, 2014), None),
    'categorias' : ['aula', 'lazer', 'musica']
})
