# coding:utf-8
output = None
pontos = 1.2

def abrir(arquivo):
	global output
	output = open(arquivo, 'w')
	output.write('<meta charset="utf-8">\n')

class Iteracoes(object):
	def __init__(self, iteracoes = 1):
		self.iteracoes = iteracoes

	def __call__(self, funcao):
		def decorador(*args, **kwargs):
			for i in range(self.iteracoes):
				funcao(*args, **kwargs)
		return decorador

class Relatorio(object):
	def __init__(self, itens):
		self.itens = itens

	def __call__(self, funcao):
		def decorador(*args, **kwargs):
			try:
				return funcao(*args, **kwargs)
			except AssertionError as error:
				output.write('<h2>Funcão "' + funcao.__name__.replace('teste_', '') + '"apresentou um erro (%.2f)</h2>\n' % (-1.0 / self.itens))
				output.write('À esquerda, o valor retornado por ela e à direita, o valor esperado:<br>\n')
				output.write('<code>\n' + str(error).replace('\n', '<br>\n') + '\n</code>\n')
				#raise error
			except AttributeError as error:
				output.write('<h2>Funcão "' + funcao.__name__.replace('teste_', '') + '"não implementada (%.2f)</h2>\n' % (-1.0 / self.itens))
				#raise error
			except Exception as error:
				output.write('<h2>Funcão "' + funcao.__name__.replace('teste_', '') + '"apresentou erro de execução (%.2f)</h2>\n' % (-1.0 / self.itens))
				output.write('<code>\n' + str(error).replace('\n', '<br>\n') + '\n</code>\n')
				#raise error
		return decorador
