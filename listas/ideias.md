
#Listagem de conteúdos#
##Linguagem Python##

###Básico###

__Sintaxe e fluxo de controle/dados__

* Expressões e variáveis
* If
* For
* While
* Funções

__Tipos de dados__

* Numéricos
* Strings
* Tuplas
* Listas
* Conjuntos
* Dicionários

###Aditivos###

* Módulos do python
    * math
    * random
    * time
    * datetime
* Compreensão de listas
* Formatação de strings
* Entrada e saída
* Funções lambda
* Ferramentas de programação funcional

###Paradigma procedural###

* Interação
* Escopo e modularização
* Estruturas de dados

##Paradigma orientado a objetos##

* Construtores e atributos
* Métodos
* Decoradores e encapsulamento
* Exceções e duck typing
* Herança e polimorfismo
	
##Paradigma funcional##

##Algoritmos##

#Outra listagem de conteúdos#

* funções diversas
    * sorted

* módulos
    * random
        * random
        * randint
        * choice
        * shuffle
    * time
    * datetime

* sequências
    * for-each
    * in
    * fatiamento
    * reversão
    * index()
    * + e *

* list
    * append
    * sort()

* str
    * split()
    * join()
    * upper()
    * lower()
    * center()
    * ljust()
    * rjust()
    * %
    * conversões para tipos numéricos
    
* tuple
    * packing e unpacking (atribuição e for-each)

* dict
    * for-each
    * update()
    
* set
    
* funções
    * argumentos padrão
    * argumentos chave
    * \*args e \*\*kwargs
    * aninhadas (closures)

* programação funcional
    * lambda
    * map
    * reduce
    * sum
    
#As funções que inspiraram os enunciados da primeira lista#


    # coding: utf-8 #
    def parabola(x, a = 1, b = 0, c = 0):
    	"""expressões"""
    	return a * x**2 + b * x + c
    
    def modulo(x):
    	if x < 0:
    		return -x
    	else:
    		return x
    
    def amostras_inc(ini, fim, inc = 1):
    	amostras = []
    	x = ini
    	while x <= fim:
    		amostras += [x]
    		x += inc
    	return amostras
    
    def amostras_n(ini, fim, num = 100):
    	return amostras_n(ini, fim, (fim - ini) / (num - 1.0))
    
    # sum, reduce, slicing, set
    def somatorio(lista, inicial = 0):
    	"""lista,for-each,variáveis"""
    	soma = inicial
    	for x in lista:
    		soma += x
    	return soma
    
    # produtório e série
    
    def reduzir(lista, funcao):
    	"""listas,for,range,len,while,argumentos-função"""
    	if lista:
    		resultado = lista[0]
    		for i in range(1, len(lista)):
    			resultado = funcao(resultado, lista[i])
    		return resultado
    	else:
    		return None
    
    def minimo(lista):
    	"""if,for,len,range"""
    	menor = lista[0]
    	for i in range(1, len(lista)):
    		if lista[i] < menor:
    			menor = lista[i]
    	return menor
    
    def maximo(lista):
    	"""if,for,len,range"""
    	maior = lista[0]
    	for i in range(1, len(lista)):
    		if lista[i] < maior:
    			maior = lista[i]
    	return maior
    
    def achatar(lista_de_tuplas):
    	"""listas,tuplas,for"""
    	resultado = []
    	for tupla in lista_de_tuplas:
    		resultado += list(tupla)
    
    def mapear(lista, funcao):
    	"""listas,for-each,argumentos-função"""
    	resultado = []
    	for elemento in lista:
    		resultado += [funcao(elemento)]
    	return resultado
    
    def filtrar(lista, funcao):
    	"""listas,for-each,argumentos-função"""
    	resultado = []
    	for elemento in lista:
    		if funcao(elemento):
    			resultado += [elemento]
    	return resultado
    
    def fatia(lista, ini = None, fim = None, inc = None):
    	"""if,lista,for,range,while"""
    	fatia = []
    
    	if ini is None:
    		ini = 0
    
    	if fim is None:
    		fim = len(lista)
    
    	if inc is None:
    		inc = 1
    
    	for i in range(ini, fim, inc):
    		fatia += [lista[i]]
    
    	return fatia
    
    def juncao_strings(strings, separador = '\n'):
    	"""strings,range,len,for"""
    	if strings:
    		juncao = strings[0]
    		for i in range(1,len(strings)):
    			juncao += separador + strings[i]
    		return juncao
    	else:
    		return ''
    
    def remover_todos(lista, valor):
    	resultado = []
    	for elemento in lista:
    		if elemento != valor:
    			resultado += [elemento]
    	return resultado
    
    def soma_matricial(a, b):
    	c = []
    	for i in range(len(a)):
    		c += []
    		for j in range(len(a[0])):
    			c[i] += [a[i][j] + b[i][j]]
    	return c
    
    def produto_matricial(a, b):
    	c = []
    	for i in range(len(a)):
    		c += []
    		for j in range(len(b[0])):
    			s = 0
    			for k in range(len(a[0])):
    				s += a[i][k] + b[k][j]
    			c[i] += [s]
    	return c
    
    def separar(texto, separador):
    	"""listas,strings,for-each,while"""
    	lista = []
    	palavra = ''
    	for letra in texto:
    		if letra == separador:
    			lista += [palavra]
    			palavra = ''
    		else:
    			palavra += letra
    	if palavra and palavra != separador:
    		lista += [palavra]
    	return lista
    
    def conjunto(lista):
    	resultado = []
    	for elemento in listas:
    		if elemento not in resultado:
    			resultado += [elemento]
    	return resultado


##Idéias de exercícios OO

1. Polinômios
1. Corrida de carros
1. Paraquedismo
1. Agenda
1. Cálculo de rotas — mais curta, mais breve, mais barata, mais fácil
1. Livro de receitas
1. Coisas para se programar em orientação a objetos — sugestão [daqui](http://www.perlmonks.org/?node_id=152358)
    * Caça-níqueis
    * Cafeteira
    * Lanterna
