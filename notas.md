##Assunto 1 - Introdução e revisão de programação procedural em Python##

###Aula teórica###
Alguns links diversos que eu anotei:

__Educação__

* [Inteligências se aprende](http://livraria.folha.com.br/livros/educacao/inteligencia-aprende-patricia-konder-lins-silva-1171766.html), livro de Patrícia Konder Lins e Silva
* [Escola parque](http://www.escolaparque.g12.br/escola/instituicao/artigos.php) -- artigos diversos sobre educação de uma escola visionária
* [A Mente Humana e a Nova Ciência do Aprendizado - Aprendizagem Significativa](https://www.youtube.com/watch?v=jAxDJx-S0B0)
* [A ciência do aprendizado](http://www.methodus.com.br/artigo/329/a-ciencia-do-aprendizado.html)
* [Por que estudar se nunca vou precisar disso?](http://www.institutoinsight.net/por-que-estudar-se-nunca-vou-precisar-disso/)
* [É hora de reorganizar tempo e espaço da sala de aula](http://porvir.org/porpessoas/e-hora-de-reorganizar-tempo-espaco-da-sala-de-aula/20130409)

__Por que programar?__

* [Why programming teaches so much more than technical skills](http://blogs.kqed.org/mindshift/2013/05/why-programming-teaches-so-much-more-than-technical-skills/)
* [How deprogramming kids from how to do school could improve learning](http://ww2.kqed.org/mindshift/2014/12/15/how-deprogramming-kids-from-how-to-do-school-could-improve-learning/) -- narra as experiências de um professor de física e matemática de nível médio norte-americano, que adotou um abordagem focada em cooperação e aprimoramento. Cheio de boas referências ao longo e ao final do texto.
* [Three awesome apps that help kids make games](http://blogs.kqed.org/mindshift/2014/06/three-awesome-apps-that-help-kids-make-games/)
* [Why programming is important?](https://www.youtube.com/watch?v=Dv7gLpW91DM)
* [Why program?](http://www.cprogramming.com/whyprogram.html)
* [Why programmiing is a good medium for expressing poorly  understood and sloppy formulated ideas](http://web.media.mit.edu/~minsky/papers/Why%20programming%20is--.html)
* [Why programming is the core skill of the 21st century](http://readwrite.com/2013/05/31/programming-core-skill-21st-century)

__Recursos para aprender programação__

* [Pycursos](http://pycursos.com/)
* [Math is fun](https://www.mathsisfun.com/)
* [5 project ideas to help you learn programming faster](http://www.makeuseof.com/tag/5-project-ideas-help-learn-programming-faster/)
* [1000 beginner programming projects](http://www.reddit.com/r/learnprogramming/comments/2a9ygh/1000_beginner_programming_projects_xpost/)
* [Want to learn a new language? Solve these 100 projects, and you'll be the best damn coder](http://www.reddit.com/r/programming/comments/1isd63/want_to_learn_a_new_language_solve_these_100/)

###Aula prática###
Os problemas sobre calendário são simples e estimulam a programação modular. Porém, a falta de conhecimentos básicos de programação e de python que constatei nesta etapa podem tornar até estes exercícios trabalhosos. Aparentemente, os alunos chegam com muitos tijolos soltos e pouca ou nenhuma capacidade de usá-los pra edificar algo concreto. Alguns pontos comuns de confusão:

* A definição de uma função não causa a sua execução imediata
* Diferença entre executar um programa python e usar o modo interativo do interpretador
* Uma função pode utilizar outras funções já usadas
* Expressões booleanas não definem verdades, mas testam condições
* O que muitos comandos esperam são uma expressão ou lista de expressões e expressões podem ser compostas por outras expressões
* Falar em programação procedural sem falar em pilhas de chamadas é como ensinar a dirigir sem mencionar mecânica, mas é como os alunos vem de Comp I: não têm idéia de como funciona a execução de um programa

Isto me faz pensar que talvez algumas sugestões recorrentes que eu recebi sejam eficazes, como a de passar numerosos exercícios para codificar funções simples, o que auxiliaria a dominar os comandos básicos e começar a entender o fluxo de execução. Programar no entanto, não consiste apenas em escrever pequenos blocos, mas em saber combiná-los e saber quando dividir ou quando repetir.

Algumas dificuldades que os estudantes tem me parecem interentes ao Python:

* Alto nível — a sintaxe simples é boa, mas a semântica alto nível das operações requer pensamento abstrato. Só consegue abstrair quem já possui noções concretas, coisa que ninguém alcança partindo de uma linguagem tão abstrata. O melhor ambiente para começar tem que ser o mais restrito possível, como resolver quebra-cabeças e problemas usando ferramentas delimitadas: aprendizado McGayver
* Duck typing — é difícil explicar a alguém que um parâmetro deve ser inteiro ou string ou etc. sem tipos estáticos ou sem falar em exceções

Quanto aos problemas sobre o roteiro de viagens, estes são muito difíceis para esta etapa. O enunciado de número 7 envolve a manipulação de estruturas de dados demasiado complexas, de forma que, sendo o último exercício fora o bônus, eu já não tinha paciência pra resolvê-lo e ficou sem solução mesmo.

##Assunto 2 - Depuração e testes automáticos##
###Aula teórica###
A apresentação destes assuntos se revelou precoce. Foi feita neste momento com o
intuito de fornecer mais uma semana para revisar os conteúdos relativos a
Computação I, mas ficou claro que a capacidade dos alunos com relação não apenas
ao domínio de uma linguagem formal, como à resolução algorítmica de problemas e
modelagem matemática são muito imaturas a esta altura. Por isso, ensinar uma
metodologia de desenvolvimento madura e criteriosa não faz sentido para quem
sequer consegue resolver problemas simples, quem dirá projetar programas
inteiros com funcionalidade modularizada.

O uso do Pythontutor pode ser muito mais interessante, pois permite explicar a
eles como a execução de um programa é extremamente não linear, em relação ao
código-fonte escrito. Esta noção se revelou uma novidade ou obscuridade para
muitos e a distinção entre a _definir_ e _chamar_ uma função talvez ainda não
esteja clara para alguns.

O assunto de depuração ficou pendente e foi pouco explorado. Antes que eles
façam programas realmente extensos, não faz sentido falar em depuração, por mais
que esta auxilie no processo de resolução de problemas.

###Aula prática###
Instalar o nose é um tormento em computadores com Windows sem acesso de
administrador. Nem sempre o `pip` vem instalado ou nem sempre é possível
instalar sem permissão de administrador. A solução é chamar as funções de teste
manualmente, uma a uma, o que (ironicamente) acaba sendo muito mais simples para
os alunos.

O maquinário de testes não ficou claro para eles. Muitos não compreenderam a
diferença entre testar e codificar, pois entenderam que as condições dos testes
_especificam_ as respostas que a função testada _dá_ naquelas situações e não
que _verificam_ quais respostas _devem_ ser dadas. Quase ninguém usou depuração
para entender o funcionamento de seus programas e procurar por erros. 

Alguns conceitos são muito mal compreendidos:

* Um módulo é uma coleção de funções
* O conteúdo de um módulo é acessado usando seu nome (ou apelido), seguindo por um ponto
* Um módulo precisa ser importado antes de usado
* Um módulo nada mais é do que um arquivo
* Definir uma função não o mesmo que chamá-la
* Variáveis possuem escopo
* Uma função tem semântica, então não faz sentido retornar mensagens onde o esperado é um número, por exemplo — eles abusam de `print`
* Faz falta a compreensão de uma "cartilha de truques básicos", como
	* Reversão de sequências
	* Empacotamento e desempacotamento de tuplas
	* Formatação de strings

A compreensão de macro-programação é deficitária e isto faz com que eles se
enrolem na hora da micro-programação — por exemplo, a mania de usar `print` ou
retornar strings formatadas ao invés do tipo correto — pois não conseguem
compreender que entrada, saída e processamento são fases distintas que devem ser
realizadas por módulos/funções/rotinas distintos e encadeados.
